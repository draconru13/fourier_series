using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            SetSize();
        }

        int Apl = 50; 
        int Faus = 1; 
        int nterms = 1; 
        private Graphics g;
        public Pen myPen = new Pen(Color.Black);
        private int Red, Green, Blue, Trans;
        private void ApplySet()
        {
            myPen.Color = Color.FromArgb(Red, Green, Blue);
        }


        private void button3_Click(object sender, EventArgs e)
        {
            if (i == 0)
ytemp = pictureBox1.Height / 4 + (int)Math.Truncate(yp);
g.DrawLine(myPen, xtemp, ytemp, i, pictureBox1.Height / 4 + (int)Math.Truncate(yp));
xtemp = i;
ytemp = pictureBox1.Height / 4 + (int)Math.Truncate(yp);
yp = 0;
}

}

pen.Color = Color.Black;
g.Dispose();
pictureBox1.Invalidate();

            }

        }

        }

        private void Form7_Resize(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
            this.button1_Click(sender, e);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Green = Convert.ToInt32(trackBar1.Value);
            ApplySet();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            Blue = Convert.ToInt32(trackBar3.Value);
            ApplySet();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {

            Red = Convert.ToInt32(trackBar2.Value);
            ApplySet();
        }


        private void redrawFourier()
        {
            int line = pictureBox1.Width;
            int helpmd = pictureBox1.Height;
            Abdf =  (int)numericUpDown3.Value;
            Faus = (int)numericUpDown4.Value;
            nterms = (int)numericUpDown4.Value;
            myPen.Width = Convert.ToInt32(numericUpDown1.Value);
            switch (comboBox2.SelectedIndex)
            {

                case 0:
                    myPen.DashStyle = DashStyle.Solid;
                    break;
                case 1:
                    myPen.DashCap = DashCap.Round;
                    myPen.DashPattern = new float[] { 4.0F, 2.0F, 1.0F, 3.0F };
                    myPen.DashStyle = DashStyle.Dot;
                    break;
                case 2:
                    myPen.DashCap = DashCap.Round;
                    myPen.DashPattern = new float[] { 4.0F, 2.0F, 1.0F, 3.0F };
                    myPen.DashStyle = DashStyle.DashDot;
                    break;
                case 3:
                    myPen.DashCap = DashCap.Round;
                    myPen.DashPattern = new float[] { 4.0F, 2.0F, 1.0F, 3.0F };
                    myPen.DashStyle = DashStyle.DashDotDot;
                    break;
            }
	    
 	    //create picturebox
            Bitmap btmp = new Bitmap(l, h);
            pictureBox1.Image = btmp;
            Image bmp = pictureBox1.Image;
            g = Graphics.FromImage(bmp);
            g.Clear(pictureBox1.BackColor);
            myPen.Color = Color.Black;
            g.DrawLine(myPen, 0, h / 2, l, h/ 2);
            g.DrawLine(myPen, l / 2, 0, l / 2, h);
            int Interval = l;
            double yp = 0, yy1 = 0, yy2 = 0;
            int angle = 0;
            int xtemp = 0;
            int ytemp = h / 2;
            myPen.Color = Color.FromArgb(Red, Green, Blue);
            myPen.DashStyle = DashStyle.Solid;
          
            if (radioButton3.Checked)
            {
                for (int i = 0; i < Interval; i++)
                {

                    for (int j = 1; j < nterms; j++)

                    {
                        y = counter / ((2 * j) - 1);
                        double arg = ((j * 2) - 1) * F * 0.01397 * angle;
                        y = Math.Sin(arg);
                    }
                    g.DrawLine(myPen, xtemp, ytemp, i, h / 2 + (int)Math.Truncate(yp));
                    xtemp = i;
                    ytemp = h / 2 + (int)Math.Truncate(yp);
                    yp = 0;
                    angle++;
                }
            }
            if (radioButton1.Checked)
            {
                for (int i = 0; i < Interval; i++)
                {

                    for (int j = 1; j < nterms; j++)

                    {
                        yy1 = (2 * A * Math.Pow(-1, j)) / (Math.PI * j);
                        double arg = j * F * 0.01397 * angle;
                        yy2 = Math.Sin(arg);
                    }
                    g.DrawLine(myPen, xtemp, ytemp, i, h / 2 + (int)Math.Truncate(yp));
                    xtemp = i;
                    ytemp = h / 2 + (int)Math.Truncate(yp);
                    yp = 0;
                    angle++;
                }
            }
            if (radioButton2.Checked)
            {
                for (int i = 0; i < Interval; i++)
                {

                    for (int j = 1; j < nterms; j++)

                    {
                        y = 8 * A * Math.Pow(-1, (2 * j - 2) / 2) / Math.Pow((Math.PI * ((2 * j) - 1)), 2);
                        double arg = ((j * 2) - 1) * F * 0.01397 * angle;
                        y = Math.Sin(arg);
                    }
                    g.DrawLine(myPen, xtemp, ytemp, i, h / 2 + (int)Math.Truncate(yp));
                    xtemp = i;
                    ytemp = h / 2 + (int)Math.Truncate(yp);
                    yp = 0;
                    angle++;
                }
            }
            g.Dispose();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            redrawFourier();
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private bool mouse = false;
         
        private class ArrayPoints
        {
            private int index = 0;
            private Point[] points;

            public ArrayPoints(int size)
            { if (size <= 0) { size = 2; }
                points = new Point[size];
            }
            public void SetPoint(int x, int y)
            {
                if(index >= points.Length)
                {
                    index = 0;
                }
                points[index] = new Point(x, y);
                index++;
            }

            public void ResetPoints()
            {
                index = 0;
            }
            public int GetCountPoints()
            {
                return index;
            }
            public Point[] GetPoints()
            {
                return points;
            }
        }
   
        }

        private void SetSize()
        {
            Pen pen = new Pen(Color.Red);
            Rectangle rectangle = Screen.PrimaryScreen.Bounds;
            map = new Bitmap(rectangle.Width, rectangle.Height);
            graphics = Graphics.FromImage(map);

            pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            
   	}

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Pen pen = new Pen(Color.Red);
            if (radioButton4.Checked)
            {
                if (!mouse)
                {
                    return;
                }
                arr1.SetPoint(e.X, e.Y);
                if (arr1.GetCountPoints() >= 2)
                {
                    graphics.DrawLines(myPen, arr1.GetPoints());
                    pictureBox1.Image = map;
                    arr1.SetPoint(e.X, e.Y);
                }
            }
        }

        private void Form7_Load(object sender, EventArgs e)
        {

        }
    }
    
}
